import scrapy
import json
import re


class TrackSpider(scrapy.Spider):
    name = "grab"

    def start_requests(self):
        f = open('link.json',)
        data = json.load(f) 
        # urls = [
        #     'https://www.detik.com/tag/kerumunan/?sortby=time&page={}'.format(i+1) for i in range(10)
        # ]
        for url in data[0:5]:
            yield scrapy.Request(url=url['url'], callback=self.parse)

    def parse(self, response):
        # match = re.search(r'(\d+/\d+/\d+)', response.css('head::text'))
        date = None
        try:
            date = response.xpath('string(//body)').re(r"(\d+/\d+/\d+)")
        except:
            pass

        yield {"waktu": date, "title" : response.css('title::text').get()}
