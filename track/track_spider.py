import scrapy


class TrackSpider(scrapy.Spider):
    name = "track"

    def start_requests(self):
        urls = [
            'https://www.detik.com/tag/kerumunan/?sortby=time&page={}'.format(i+1) for i in range(10)
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for article in response.css('article'):
            yield { "url": article.css('a::attr(href)').get() }
